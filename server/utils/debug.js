const chalk = require('chalk');

const oops = (err)=>{
    console.log(`${chalk.redBright('Oops I have a ERROR')}`);
    console.log(err);
}

module.exports = {
    oops,
}