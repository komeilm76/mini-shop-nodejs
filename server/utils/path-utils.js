const path = require("path");

const rootDir = process.cwd();
const serverDir = path.join(rootDir, "server");
const modelsDir = path.join(serverDir, "models");
const viewsDir = path.join(serverDir, "views");
const controllersDir = path.join(serverDir, "controllers");
const utilsDir = path.join(serverDir, "utils");
const routesDir = path.join(serverDir, "routes");
const publicDir = path.join(serverDir, "public");
const databaseDir = path.join(serverDir, "database");
const themesDir = path.join('themes');

const staticDirS = (express , app) => {
    app.use(express.static(path.join(publicDir)));
};

module.exports = {
  rootDir,
  serverDir,
  modelsDir,
  viewsDir,
  controllersDir,
  utilsDir,
  routesDir,
  publicDir,
  databaseDir,
  themesDir,
  staticDirS
};
