// internal modules
const fs = require("fs");
const path = require("path");
// internal modules
// ----------
// external modules
const express = require("express");
const app = express();
const { urlencoded } = require("body-parser");
let checkDB = true;

// external modules
// ----------
// my modules
const {
  rootDir,
  serverDir,
  staticDirS,
  databaseDir,
  viewsDir,
  themesDir,
} = require("./utils/path-utils");
const clientRoutes = require("./routes/client/client-routes");
const DB = require("./database/database-config");
// require('./models/product-models');
// my modules
// ----------
// variables
const port = 3000;
// variables
// ----------
// template engine
app.set("view engine", "ejs");
app.set("views", "server/views");
// template engine
// ----------
// middlewares

app.use(urlencoded({ extended: false }));
// middlewares
// ----------
// statics
staticDirS(express, app);
// statics
// ----------
// routes
app.use((req, res, next) => {
  if (checkDB) {
    next();
  } else {
    res.render(path.join(viewsDir, "error-db"), {
      pageTitle: "error-db",
      themeDir: path.join(themesDir, "amado-master/"),
    });
  }
});
app.use(clientRoutes);

// routes
// ----------

DB.sync()
  .then((result) => {
    console.log("success on DB.sync()");
    console.log(checkDB);
  })
  .catch((err) => {
    console.log("err on DB.sync()");
    checkDB = false;
    console.log(checkDB);
  })
  .finally((onFinally) => {
    app.listen(port, () => {
      console.log(
        `server is running on http://localhost:${port} | DATABASE : DIactive`
      );
    });
  });

// running server on port

// running server on port
