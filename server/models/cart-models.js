const { DataTypes } = require('sequelize');

const  sequelize = require('../database/database-config');

const cartModel = sequelize.define("cart" , {
    id:{
        type:DataTypes.INTEGER,
        autoIncrement:true,
        primaryKey:true,
        allowNull:false,
    },
    productUuid:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    productImage:{
        type:DataTypes.STRING,
        allowNull:true,
    },
    productName:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    productPrice:{
        type:DataTypes.INTEGER,
        allowNull:true,
        defaultValue:0,
    },
    orderCount:{
        type:DataTypes.INTEGER,
        allowNull:false,
        defaultValue:1,
    },
});

module.exports = cartModel;