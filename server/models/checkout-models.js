const {DataTypes, Sequelize} = require('sequelize');

const sequelize = require('../database/database-config');

const checkoutModel = sequelize.define('order' , {
    id:{
        type:DataTypes.INTEGER,
        autoIncrement:true,
        primaryKey:true,
        allowNull:false,
    },
    userFirstName:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    userLastName:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    userEmail:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    userAddress:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    userOrders:{
        type:DataTypes.STRING,
        allowNull:false,
        
    }
});


module.exports = checkoutModel;