const { DataTypes } = require('sequelize');

const  sequelize = require('../database/database-config');

const shopModel = sequelize.define("product" , {
    id:{
        type:DataTypes.INTEGER,
        autoIncrement:true,
        primaryKey:true,
        allowNull:false,
    },
    uuid:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    productName:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    price:{
        type:DataTypes.INTEGER,
        allowNull:false,
    },
    imageUrl:{
        type:DataTypes.STRING,
        allowNull:true,
    },
    imageUrl2:{
        type:DataTypes.STRING,
        allowNull:true,
    },
    category:{
        type:DataTypes.STRING,
        allowNull:true,
    },
    state:{
        type:DataTypes.STRING,
        allowNull:false,
        defaultValue:'avalible', 
    },
    productId:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    brand:{
        type:DataTypes.STRING,
        allowNull:true,
    },
    description:{
        type:DataTypes.STRING,
        allowNull:true,
    },
    
});

module.exports = shopModel;