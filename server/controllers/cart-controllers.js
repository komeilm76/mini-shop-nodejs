const cartModel = require("../models/cart-models");
const productModel = require("../models/product-models");

const addProductToCart = (req, res, data) => {
  console.log("uuid", req.params.id);
  productModel.findOne({ where: { uuid: req.params.id } }).then((product) => {
    cartModel
      .create({
        productUuid: product.uuid,
        productName: product.productName,
        productImage: product.imageUrl,
        productPrice: product.price,
        orderCount: req.body.count,
      })
      .then((result) => {
        console.log("product is added to cart");
      });
  });
  data();
};

const ordersList = (req, res, data) => {
  cartModel.findAll().then((orders) => {
    data(orders);
  });
};

const deleteOrder = (req, res, data) => {
  cartModel.destroy({ where: { productUuid: req.params.id } }).then(result=>{
      console.log('order is deleted');
  })
};

module.exports = {
  addProductToCart,
  ordersList,
  deleteOrder
};
