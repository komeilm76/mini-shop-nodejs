const { v4: uuidV4 } = require("uuid");
const productModel = require("../models/product-models");

const addProduct = (req, res) => {
  productModel
    .create({
      uuid: uuidV4(),
      productName: req.body["product-name"],
      price: req.body["price"],
      imageUrl: req.body["image-url"],
      imageUrl2: req.body["image-url2"],
      category: req.body["category"],
      state: req.body["state"],
      productId: req.body["product-id"],
      brand: req.body["brand"],
      description: req.body["description"],
    })
    .then((result) => {
      res.redirect("/");
    })
    .catch((err) => console.log(err));
};

const getAllProduct = (req, res, data) => {
  productModel.findAll().then((products) => {
    data(products);
  });
};

const getSelectedProduct = (req, res, data) => {
  productModel.findOne({where: { uuid: req.params.id }}).then((product) => {
    data(product);
  });
};

const deleteSelectedProduct = (req , res , data)=>{
    productModel.destroy({where:{uuid:req.params.id}}).then(product=>{
        data(product);
        
    })
}

module.exports = {
  addProduct,
  getAllProduct,
  getSelectedProduct,
  deleteSelectedProduct,
};
