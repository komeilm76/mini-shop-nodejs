const checkoutModel = require('../models/checkout-models');
const cartModel = require('../models/cart-models');

const checkoutSubmit = (req , res , data)=>{
    cartModel.findAll({attributes:['productUuid']}).then(orders=>{
        checkoutModel.create({
            userFirstName : req.body['first_name'],
            userLastName : req.body['last_name'],
            userEmail : req.body['email'],
            userAddress : req.body['address'],
            userOrders : JSON.stringify(orders),
        }).then(result=>{
            console.log('cheched out is completed');
        })
    })
    
}

const alertOfSubmit = (req , res , data)=>{
    checkoutModel.findAll().then(userOrder=>{
        data(userOrder);
    })
}

module.exports = {
    checkoutSubmit,
    alertOfSubmit
}