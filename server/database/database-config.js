const { Sequelize } = require("sequelize");

const DBName = "Please insert your Database Name Here";
const DBUser = "Please insert your Database User Here";
const DBPassword = "Please insert your Database Password Here";



const sequelize = new Sequelize(DBName, DBUser, DBPassword, {
  dialect: "mysql", //yourDataBase
  host: "localhost", //yourDomain
});

module.exports = sequelize;
