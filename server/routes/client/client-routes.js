const path = require("path");
const express = require("express");
const { viewsDir, themesDir } = require("../../utils/path-utils");
const {
  addProduct,
  getAllProduct,
  getSelectedProduct,
  deleteSelectedProduct,
} = require("../../controllers/product-controllers");
const {
  addProductToCart,
  ordersList,
  deleteOrder,
} = require("../../controllers/cart-controllers");
const {
  checkoutSubmit,
  alertOfSubmit,
} = require("../../controllers/chechout-controllers");
const router = express.Router();

router.get("/", (req, res) => {
  getAllProduct(req, res, (data) => {
    console.log(data);
    res.render(path.join(viewsDir, "index"), {
      products: data,
      themeDir: path.join(themesDir, "amado-master/"),
    });
  });
});


router.get("/shop", (req, res) => {
  getAllProduct(req, res, (data) => {
    console.log(data);
    res.render(path.join(viewsDir, "shop"), {
      products: data,
      themeDir: path.join(themesDir, "amado-master/"),
    });
  });
});

router.get("/product/:id", (req, res) => {
  getSelectedProduct(req, res, (data) => {
    res.render(path.join(viewsDir, "product"), {
      pageTitle: "Product",
      themeDir: path.join(themesDir, "amado-master/"),
      product: data,
    });
  });
});
router.get("/product/delete-product/:id", (req, res) => {
  deleteSelectedProduct(req, res, (data) => {
    res.render(path.join(viewsDir, "product"), {
      product: data,
      themeDir: path.join(themesDir, "amado-master/"),
    });
  });
  res.redirect("/");
});

router.post("/product/add-to-cart/:id", (req, res) => {
  addProductToCart(req, res, () => {});
  res.redirect("/cart");
});

router.get("/cart", (req, res) => {
  ordersList(req, res, (data) => {
    const getTotal = () => {
      let price = 0;
      let count = 0;
      for (item of data) {
        price = price + item.productPrice;
        count = count + item.orderCount;
      }
      return { price, count };
    };
    res.render(path.join(viewsDir, "cart"), {
      pageTitle: "Checkout",
      themeDir: path.join(themesDir, "amado-master/"),
      orders: data,
      total: getTotal(),
    });
  });
});
router.get("/cart/remove-from-cart/:id", (req, res) => {
  deleteOrder(req, res, (data) => {});
  res.redirect("/cart");
});

router.get("/checkout", (req, res) => {
  res.render(path.join(viewsDir, "checkout"), {
    pageTitle: "Checkout",
    themeDir: path.join(themesDir, "amado-master/"),
  });
});
router.post("/checkout-finished", (req, res) => {
  checkoutSubmit(req, res);
  res.redirect("/order-alert");
});

router.get("/order-alert", (req, res) => {
  alertOfSubmit(req, res, (data) => {
    res.render(path.join(viewsDir, "order-alert"), {
      pageTitle: "Order Alert",
      userOrders: data,
      themeDir: path.join(themesDir, "amado-master/"),
    });
  });
});

router.get("/new-product", (req, res) => {
  res.render(path.join(viewsDir, "new-product"), {
    pageTitle: "Checkout",
    themeDir: path.join(themesDir, "amado-master/"),
  });
});
router.post("/create-new-product", (req, res) => {
  addProduct(req, res);
});

module.exports = router;
